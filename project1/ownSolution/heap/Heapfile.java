/* File Heapfile.java */

package heap;

import java.io.*;
import java.lang.*;

import global.*;
import bufmgr.*;
import diskmgr.*;


public class Heapfile implements GlobalConst
{
	PageId	_firstDirPageId;
	String	heapFileName;


  /* Create a Heap File (constructor for Heapfile class) */
  public Heapfile(String name) 
  {
	String _default_name = "heapfile_def";

	if (name == null){
		heapFileName = _default_name;
	} else {
		heapFileName = name;
	}

	try {		
		_firstDirPageId = SystemDefs.JavabaseDB.get_file_entry(name);

		if (_firstDirPageId == null){
			// create a new heapfile in the database
			Page dirPage = new Page();
			HFPage DirHeaderPage = new HFPage();
			_firstDirPageId = SystemDefs.JavabaseBM.newPage(dirPage,1);
			SystemDefs.JavabaseDB.add_file_entry(name,_firstDirPageId);
			DirHeaderPage.init(_firstDirPageId,dirPage);
			SystemDefs.JavabaseBM.unpinPage(_firstDirPageId, true);
		}

	} catch (Exception e) {
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}
  }




/* Return number of records in file */
public int getRecCnt()
{
	int numberRecords;		
	numberRecords = 0;

	try {
		PageId dirPid = new PageId();
		HFPage dirPage = new HFPage();
		RID dirRid = new RID();
		
		for (dirPid=_firstDirPageId; dirPid.pid!=INVALID_PAGE; dirPid=dirPage.getNextPage())
		{
			SystemDefs.JavabaseBM.pinPage(dirPid, dirPage, false);
			
			for(dirRid=dirPage.firstRecord(); dirRid!=null; dirRid=dirPage.nextRecord(dirRid))
			{
				Tuple tuple = dirPage.getRecord(dirRid);
				DataPageInfo dpInfo = new DataPageInfo(tuple);
				numberRecords = numberRecords + dpInfo.recct;
			}
			SystemDefs.JavabaseBM.unpinPage(dirPid,false);
		}
	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}

	return numberRecords;

}





/* Insert record into file, return its Rid */
public RID insertRecord(byte recPtr[]) 
{ 

	RID datapageRid = new RID();

  	try {

  		HFPage dirPage = new HFPage();
  		PageId dirPid = new PageId();
  		PageId nextDirPid = new PageId();
  		PageId nextDirPagePid = new PageId();
  		RID dirRid = new RID();
  		boolean goThroughSlots;
  		boolean lastDirPage = false;
		boolean recordInserted = false;		
  		int counter = 0;
  		
  		int pageNumber = INVALID_PAGE;
  		
  		System.out.println("INSERT RECORD");
  		
  		for (pageNumber=_firstDirPageId.pid; pageNumber!=INVALID_PAGE; pageNumber=pageNumber)
  		{
			if (recordInserted){
				break;
			}

			
			if (pageNumber == INVALID_PAGE){
				break;
			} else {
				dirPid.pid = pageNumber;
System.out.println("this page number: "+pageNumber);
				pageNumber = dirPage.getNextPage().pid;
System.out.println("next page number: "+pageNumber);
			}

			SystemDefs.JavabaseBM.pinPage(dirPid,dirPage,false);
			System.out.println("pinning page w/pid: "+dirPid+ " nextpage is "+dirPage.getNextPage());

			if (dirPage.empty()){
				System.out.println("empty dirpage!");
			}

  			if ( dirPage.getNextPage().pid==INVALID_PAGE  &&  dirPid.pid!=_firstDirPageId.pid ){
				System.out.println("page w/pid: "+dirPid+" is a lastDirPage");
  				lastDirPage = true;
  			} else if (dirPid.pid==_firstDirPageId.pid && dirPage.available_space() < 16 && dirPage.getNextPage().pid==INVALID_PAGE){
				System.out.println("firstpage w/pid: "+dirPid+" becomes a lastDirPage (it's full and havent a follower/next page)");
  				lastDirPage = true;
  			} else if (dirPage.available_space() < 16){
  				SystemDefs.JavabaseBM.unpinPage(dirPid,false);
  				continue;
  			}

  			dirRid=dirPage.firstRecord();
			goThroughSlots = true;




			if (dirRid!=null){
				System.out.println("dirpage.firstrecord: "+dirRid.pageNo+" slotNo: "+dirRid.slotNo);
			} else {
				System.out.println("dirpage.firstrecord == null ");
			}

  			while (goThroughSlots)
  			{
//				System.out.println("here??? ");

  				// add a new slot
  				if (dirRid==null || lastDirPage){

//					System.out.println("Add new slot in dirPid: "+dirPid.pid+" (dirPage.availspace: "+dirPage.available_space()+")");

					// not enough space for new slot, and in last page: add new dir page w/slot & dPage    or
					// empty header page: add a new slot w/dPage only
					if ( (dirPage.available_space() < 16 && lastDirPage ) )
					{
						System.out.println("Firstpage is empty or full, or lastpage is full");
						if (dirPage.available_space() < 16 && lastDirPage) // KUN ny dirpage dersom alle eksisterende er fulle!!!
						{
							System.out.println("Dir page w/pid: "+dirPid+" is full, create a new one");
							
							PageId currentPid = new PageId( dirPage.getCurPage().pid );
							PageId newPid = new PageId();
							
							HFPage newDirPage = new HFPage();
							Page initPage = new Page();
							
							newPid = SystemDefs.JavabaseBM.newPage(initPage,1);
							newDirPage.init(newPid, initPage);
							
							newDirPage.setPrevPage(currentPid);
							dirPage.setNextPage(newPid);
							
							SystemDefs.JavabaseBM.unpinPage(currentPid, true);
							SystemDefs.JavabaseBM.unpinPage(newPid, true);
							
							SystemDefs.JavabaseBM.pinPage(newPid,dirPage,false);
							
							dirPid = newPid;
							
							/*

							// create a new directory page
							int pid = dirPage.getCurPage().pid;
							PageId prevPid = new PageId(pid);
							HFPage prevPage = dirPage;
							Page tmpPage = new Page();
							dirPid = SystemDefs.JavabaseBM.newPage(tmpPage,1);
							dirPage.init(dirPid,tmpPage);
							System.out.println("new page w/pid: "+dirPid+" and available space: "+dirPage.available_space()+" added, prev. page.pid: "+prevPid);

							dirPage.setPrevPage(prevPid);						
							prevPage.setNextPage(dirPid);
							SystemDefs.JavabaseBM.unpinPage(prevPid, true);
							System.out.println("unpin previous page w/pid: "+prevPid);
							*/
							lastDirPage = false;
					
						}
					}
					
					// page is full, but it's not the last page
					if ( dirPage.available_space() < 16)
					{
						goThroughSlots=false;
						
					} else {

						// create a new datapage
						HFPage dataPage = new HFPage();
						Page tmpDataPage = new Page();
						PageId dataPagePid = SystemDefs.JavabaseBM.newPage(tmpDataPage,1);
						dataPage.init(dataPagePid,tmpDataPage);
						SystemDefs.JavabaseBM.unpinPage(dataPagePid, true);
						System.out.println("New datapage with id: "+dataPagePid+" created");
						// create a new slot
						DataPageInfo dpInfo = new DataPageInfo();
						dpInfo.availspace = dataPage.available_space();
						dpInfo.recct = 0;
						dpInfo.pageId = dataPagePid;
						Tuple tuple = dpInfo.convertToTuple();
						byte bytes[] = tuple.returnTupleByteArray();
						dirRid = dirPage.insertRecord(bytes);

					}
				} // if rid== null


//				System.out.println("Checking for available space in dirRid = pageNo: "+dirRid.pageNo+" slotNo: "+dirRid.slotNo);

				Tuple tuple = dirPage.returnRecord(dirRid);

//				System.out.println("tuple.length: "+tuple.getLength() );
				DataPageInfo dpInfo = new DataPageInfo(tuple);
//				System.out.println("... is datalength; " +Integer.toString(recPtr.length) +"<"+dpInfo.availspace+"? So far we have "+Integer.toString(dpInfo.recct) +" records in datapage ");

				
				if ( (recPtr.length+4) < dpInfo.availspace)
				{
					System.out.println("Available space in datapage w/pid: "+Integer.toString(dpInfo.pageId.pid)+" connected to dir page w/pid: "+dirPid );
					// insert into datapage
					HFPage dataPage = new HFPage();
					SystemDefs.JavabaseBM.pinPage(dpInfo.pageId, dataPage, false);
					datapageRid = dataPage.insertRecord(recPtr);
					SystemDefs.JavabaseBM.unpinPage(dpInfo.pageId, true);
					// update slot in directory page
					dpInfo.recct++;
					dpInfo.availspace = dataPage.available_space()-recPtr.length;
//					System.out.println("dpInfo byteArray.length: "+Integer.toString(dpInfo.returnByteArray().length) );
					dpInfo.flushToTuple();
					
					recordInserted = true; 
					goThroughSlots=false;
				}


  				dirRid=dirPage.nextRecord(dirRid);
  			} // while
			System.out.println("unpin page w/pid: "+Integer.toString(dirPid.pid));
  			SystemDefs.JavabaseBM.unpinPage(dirPid, true);
  		} // for




		System.out.println("Data inserted: RID = pageNo: "+datapageRid.pageNo+" slotNo: "+datapageRid.slotNo);

  		
	} catch (Exception e){
			System.out.println("Deal with exceptions later...");
			e.printStackTrace();
	}

	return datapageRid;

} // insertRecord method

/* Delete record from file with given rid. */
public boolean deleteRecord(RID rid) { 
  

	try {

		PageId pageNo = rid.pageNo;
		int slotNo = rid.slotNo;	
	
		PageId dirPid = new PageId();
		HFPage dirPage = new HFPage();
		RID dirRid = new RID();
		Tuple tuple = new Tuple();
		
		boolean recordFound = false;
		boolean dirPageDeleted = false;
		
		for (dirPid=_firstDirPageId; dirPid.pid!=INVALID_PAGE; dirPid=dirPage.getNextPage())
		{
			if (recordFound) break;
			
			SystemDefs.JavabaseBM.pinPage(dirPid, dirPage, false);
			for(dirRid=dirPage.firstRecord(); dirRid!=null; dirRid=dirPage.nextRecord(dirRid))
			{
				if (recordFound) break;

				tuple = dirPage.getRecord(dirRid);
				DataPageInfo dpInfo = new DataPageInfo(tuple);
				
				if (dpInfo.pageId == pageNo){
					HFPage dataPage = new HFPage();
					SystemDefs.JavabaseBM.pinPage(pageNo, dataPage, false);
					dataPage.deleteRecord(rid);
					SystemDefs.JavabaseBM.unpinPage(pageNo,true);

					if (dataPage.empty()){
						dirPage.deleteRecord(dirRid);
						
						if (dirPage.empty()){
							HFPage prevPage = new HFPage();
							SystemDefs.JavabaseBM.pinPage(dirPage.getPrevPage(), prevPage, false);
							PageId invalidPid = new PageId(INVALID_PAGE);
							prevPage.setNextPage(invalidPid);
							SystemDefs.JavabaseBM.unpinPage(dirPage.getPrevPage(),false);
							SystemDefs.JavabaseBM.freePage(dirPid);
						}
					} else {
					
						dpInfo.recct--;
						dpInfo.availspace = dataPage.available_space();
					
					}

					recordFound=true;
				}
	
			}
			if (!dirPageDeleted)
				SystemDefs.JavabaseBM.unpinPage(dirPid,false);
		}


		return recordFound;

	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}

  
  return false;
  
}



		
/* Updates the specified record in the heapfile. */
public boolean updateRecord(RID rid, Tuple newtuple) { 


	try {

		PageId pageNo = rid.pageNo;
		int slotNo = rid.slotNo;	
	
		PageId dirPid = new PageId();
		HFPage dirPage = new HFPage();
		RID dirRid = new RID();
		Tuple tuple = new Tuple();
		
		boolean recordFound = false;
		
		for (dirPid=_firstDirPageId; dirPid.pid!=INVALID_PAGE; dirPid=dirPage.getNextPage())
		{
			if (recordFound) break;
			
			SystemDefs.JavabaseBM.pinPage(dirPid, dirPage, false);
			for(dirRid=dirPage.firstRecord(); dirRid!=null; dirRid=dirPage.nextRecord(dirRid))
			{
				if (recordFound) break;

				tuple = dirPage.getRecord(dirRid);
				DataPageInfo dpInfo = new DataPageInfo(tuple);
				
				if (dpInfo.pageId == pageNo){
					HFPage dataPage = new HFPage();
					SystemDefs.JavabaseBM.pinPage(pageNo, dataPage, false);
					tuple = dataPage.returnRecord(rid);
					tuple.tupleCopy(newtuple);
					SystemDefs.JavabaseBM.unpinPage(pageNo,true);
					recordFound=true;
				}
	
			}
			SystemDefs.JavabaseBM.unpinPage(dirPid,false);
		}

		return recordFound;


	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}

	return false; 
	
	
}




/* Read record from file, returning pointer and length. */
public Tuple getRecord(RID rid) { 

	try {

		PageId pageNo = rid.pageNo;
		int slotNo = rid.slotNo;	
	
		PageId dirPid = new PageId();
		HFPage dirPage = new HFPage();
		RID dirRid = new RID();
		Tuple tuple = new Tuple();
		
		boolean recordFound = false;
		
		for (dirPid=_firstDirPageId; dirPid.pid!=INVALID_PAGE; dirPid=dirPage.getNextPage())
		{
			if (recordFound) break;
			
			SystemDefs.JavabaseBM.pinPage(dirPid, dirPage, false);
			for(dirRid=dirPage.firstRecord(); dirRid!=null; dirRid=dirPage.nextRecord(dirRid))
			{
				if (recordFound) break;

				tuple = dirPage.getRecord(dirRid);
				DataPageInfo dpInfo = new DataPageInfo(tuple);
				
				if (dpInfo.pageId == pageNo){
					HFPage dataPage = new HFPage();
					SystemDefs.JavabaseBM.pinPage(pageNo, dataPage, false);
					tuple = dataPage.returnRecord(rid);
					SystemDefs.JavabaseBM.unpinPage(pageNo,false);
					recordFound=true;
				}
	
			}
			SystemDefs.JavabaseBM.unpinPage(dirPid,false);
		}


		return tuple;

	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}

	return null; 
}


/* Initiate a sequential scan. */
public Scan openScan() { 
	try {
		Scan scan = new Scan(this);
		return scan;
	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}
	return null;
}


/* Delete the file from the database */
public void deleteFile() {

	try {
		PageId dirPid = new PageId();
		HFPage dirPage = new HFPage();
		RID dirRid = new RID();
		
		for (dirPid=_firstDirPageId; dirPid.pid!=INVALID_PAGE; dirPid=dirPage.getNextPage())
		{
			SystemDefs.JavabaseBM.pinPage(dirPid, dirPage, false);
			for(dirRid=dirPage.firstRecord(); dirRid!=null; dirRid=dirPage.nextRecord(dirRid))
			{
				Tuple tuple = dirPage.getRecord(dirRid);
				DataPageInfo dpInfo = new DataPageInfo(tuple);
				SystemDefs.JavabaseBM.freePage(dpInfo.pageId);
			}
			SystemDefs.JavabaseBM.unpinPage(dirPid,false);
			SystemDefs.JavabaseBM.freePage(dirPid);
		}

		SystemDefs.JavabaseDB.delete_file_entry(heapFileName);

	} catch (Exception e){
		System.out.println("Deal with exceptions later...");
		e.printStackTrace();
	}

}


};

