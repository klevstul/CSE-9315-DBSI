package btree;

import java.io.*;
import diskmgr.*;
import bufmgr.*;
import global.*;
import heap.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.StringTokenizer;


/** btfile.java
 * This is the main definition of class BTreeFile, which derives from
 * abstract base class IndexFile.
 * It provides an insert/delete interface.
 */
public class BTreeFile extends IndexFile
implements GlobalConst {

	// -------------------
	// global variables
	// -------------------
	boolean debug = false;				// print out debugging information?
	BTreeHeaderPage _headerPage;		// the BTree header page
	boolean open = false;				// a boolean to keep track of if the tree is open or closed
	KeyDataEntry _pushUpKey = null;		// if a key is "pushed up" (from recursive insert) - the value is stored here
	String _filename = null;			// the filename of the BTreeFile


 	// --------------------------------------------------
 	// constructor - open a existing B+Tree from file
 	// --------------------------------------------------
	public BTreeFile(String filename)
	throws	GetFileEntryException,
			PinPageException,
			ConstructPageException
    {
		try {
			debug("Constructor 1");
			debug("Open index '"+filename+"'");

			_filename = filename;

			// load file into a new page in bufferpool, page is pinned
			PageId pid = SystemDefs.JavabaseDB.get_file_entry(filename);

			// relate _headerPage to this new pid
			_headerPage = new BTreeHeaderPage();
			SystemDefs.JavabaseBM.pinPage(pid, _headerPage, false);

			// set the open status to true
			open = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
    }


 	// --------------------------------------------------
 	// constructor - open a new B+Tree
 	// --------------------------------------------------
	public BTreeFile(String filename, int keytype, int keysize, int delete_fashion)
	throws GetFileEntryException,
		ConstructPageException,
		IOException,
		AddFileEntryException
	{
    	try {	
			debug("Constructor 2");
			debug("Create new index '"+filename+"'");

			_filename = filename;

			// Create new headerpage (this page will get a new pageid automatically)
			_headerPage = new BTreeHeaderPage();
			PageId invalidPid = new PageId(INVALID_PAGE);
			// update information in headerpage
			_headerPage.set_deleteFashion(delete_fashion);
			_headerPage.set_keyType((short)keytype);
			_headerPage.set_maxKeySize(keysize);
			_headerPage.set_rootId(invalidPid);
			
			// Store the file in the database (page will be automatically pinned)
			SystemDefs.JavabaseDB.add_file_entry(filename, _headerPage.getPageId());

			// the B+Tree is opened
			open = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
    }
  


	// ----------------------------
	// return the headerpage
	// ----------------------------
	public BTreeHeaderPage getHeaderPage() {
		return _headerPage;
	}

  

	// ----------------------------
	// close the B+Tree
	// ----------------------------
	public void close()
	throws PageUnpinnedException,
		InvalidFrameNumberException,
		HashEntryNotFoundException,
		ReplacerException
    {
    	try {
			if(open)
			{
				SystemDefs.JavabaseBM.unpinPage(_headerPage.getCurPage(), false);
				open = false;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
    }



	// -----------------------------------------
	// destroy the file from buffermanager
	// -----------------------------------------
	public void destroyFile()
	throws IOException,
		IteratorException,
		UnpinPageException,
		FreePageException,
		DeleteFileEntryException,
		ConstructPageException,
		PinPageException
	{
		debug("destroyFile()");
		try {
			if (open)
			{
				if(_headerPage.get_rootId().pid != INVALID_PAGE)
					 SystemDefs.JavabaseBM.freePage(_headerPage.get_rootId());

				// go through all index- and leafpages and free them
				getAllRecords(new BTSortedPage(_headerPage.get_rootId(), _headerPage.get_keyType()));

				// free the headerpage
				SystemDefs.JavabaseBM.freePage(_headerPage.getCurPage());

				// the B+Tree is not opened anymore...
				open = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
  

	// ------------------------
	// insert a new record
	// ------------------------
	public void insert(KeyClass key, RID rid)
	throws	KeyTooLongException,
			KeyNotMatchException,
			LeafInsertRecException,
			IndexInsertRecException,
			ConstructPageException,
			UnpinPageException,
			PinPageException,
			NodeNotMatchException,
			ConvertException,
			DeleteRecException,
			IndexSearchException,
			IteratorException,
			LeafDeleteException,
			InsertException,
			IOException
    {
		try {
			if (!open)
				error("B+Tree is closed");
			
			debug("INSERT: key: "+key.toString());

			// check if key is of integer type
			if (BT.getKeyLength(key) != 4){
				error("Wrong key length: "+Integer.toString(BT.getKeyLength(key)));
			}
			
			// -------------------------------------------------------
			// task:	Check if headerpage has a root page, insert
			// -------------------------------------------------------
			// case 1:	B+Tree is empty and has NO root page.
			//			Add a new LeafPage as rootpage
			// case 2:	We have a root page
			//  case 2.1: The B+ Tree consists of ONLY one LeafPage (no IndexPage)
			//   case 2.1.1: The leaf page has available space for the new record
			//   case 2.1.2: The leaf page is full, have to create an index page 
			//               and split leaf page into two new pages
			//  case 2.2: The B+ Tree has a IndexPage
			// -------------------------------------------------------
			PageId rootPid = _headerPage.get_rootId();
			RID insertedRid;

			// ----------
			// CASE 1
			// ----------
			if (rootPid.pid == INVALID_PAGE){
				// create one LeafPage, and add that as a root page...
				debug("B+ Tree is empty (no root page)");
				
				// create a new LeafPage
				BTLeafPage leafPage = new BTLeafPage(_headerPage.get_keyType());
				
				//set this LeafPage as the root
				_headerPage.set_rootId(leafPage.getCurPage());
				
				// insert the record into the leafPage
				insertedRid = leafPage.insertRecord(key,rid);
				
				//unpin the page
				SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), true);

				// check that the record is inserted, otherwise stop
				if (insertedRid == null){
					error("Record is not inserted!");
				}
				
				debug("inserted rid: pagenumber: "+Integer.toString(insertedRid.pageNo.pid)+" slotNo: "+Integer.toString(insertedRid.slotNo));
				pageStat(leafPage, null);

			// ----------
			// CASE 2
			// ----------
			} else {
				debug("B+ Tree has a root page");

				// get root page
				BTSortedPage rootPage = new BTSortedPage(_headerPage.get_rootId(), _headerPage.get_keyType());
				
				
				// if page is a "Index" page, pagetype==4, 
				// else if is a "Node" page,  pagetype==8.

				// ----------
				// CASE 2.2
				// ----------
				if (getPageType(rootPage)==4){
					debug("Index Type");

					// recursiveOperation will unpin the page, so we don't have to think about that
					boolean inserted = recursiveOperation("insert", rootPage, key, rid);
					if (!inserted)
						error("insertion failed");

				// ----------
				// CASE 2.1
				// ----------
				} else if (getPageType(rootPage)==8){
					debug("Insert the record into a Node page!");
					
					// unpin root page - reopen it as a LeafPage
					SystemDefs.JavabaseBM.unpinPage(rootPage.getCurPage(), false);
					BTLeafPage leafPage = new BTLeafPage(_headerPage.get_rootId(), _headerPage.get_keyType());

					// try to insert the record
					insertedRid = leafPage.insertRecord(key,rid);
					
					// ----------
					// CASE 2.1.1
					// ----------
					if (insertedRid != null){
						SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), true);

						debug("inserted rid: pagenumber: "+Integer.toString(insertedRid.pageNo.pid)+" slotNo: "+Integer.toString(insertedRid.slotNo));
						pageStat(leafPage, "after insertion");
					// ----------
					// CASE 2.1.2
					// ----------
					} else {
						debug("LeafPage is full - have to create a new index page and split leafpages into two new");
						
						// create a new index page
						BTIndexPage indexPage = new BTIndexPage(_headerPage.get_keyType());
						
						debug("index page pid: "+Integer.toString(indexPage.getCurPage().pid) );

						// have to set type to IndexPage manually - otherwise I get 0
						indexPage.setType((short)11);
												
						// split the full leaf page into two new leafpages
						BTLeafPage newLeafPage = splitNode(leafPage);

						newLeafPage.setNextPage(new PageId(INVALID_PAGE));
						newLeafPage.setPrevPage(leafPage.getCurPage());
						
						leafPage.setNextPage(newLeafPage.getCurPage());
						leafPage.setPrevPage(new PageId(INVALID_PAGE));

						// insert the new record into new node if equal to, or bigger
						// then the first entry in the new leaf page (which is the same
						// as the value we copy up into our indexpage)
						if(BT.keyCompare(key, newLeafPage.getFirst(new RID()).key) >= 0 )
						{
							debug("record inserted into the new (rightmost) node");
							newLeafPage.insertRecord(key, rid);
						}
						// else it's less than our value copied ut in the indexpage. 
						// insert record in the old leafpage
						else
						{
							debug("record inserted into the old (leftmost) node");
							leafPage.insertRecord(key, rid);
						}
						
						// the key for the indexpage is the
						// first record of the newly created leafpage
						insertedRid = indexPage.insertKey(newLeafPage.getFirst(rid).key, newLeafPage.getCurPage());
						if (insertedRid == null){
							error("Record is not inserted into index page!");
						}

						// unpin the leafpages
						SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), true);
						SystemDefs.JavabaseBM.unpinPage(newLeafPage.getCurPage(), true);

						debug("key number "+newLeafPage.getFirst(rid).key+" is inserted into the index page");

						// set the left link to be the old leafPage
						indexPage.setLeftLink(leafPage.getCurPage());

						// change root page to become the new indexpage
						_headerPage.set_rootId(indexPage.getCurPage());
						
						// unpin the indexpage
						SystemDefs.JavabaseBM.unpinPage(indexPage.getCurPage(), true);
						
						pageStat(newLeafPage, "stat newLeafPage");
						pageStat(leafPage, "stat oldLeafPage");
					}

				} else {
					error("Unknown pagetype: "+Integer.toString(getPageType(rootPage)));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
    }
  


	// -----------------------------------------------
	// set up and return a new scan fro the B+Tree
	// -----------------------------------------------
	public BTFileScan new_scan(KeyClass lo_key, KeyClass hi_key)
	throws IOException,
		KeyNotMatchException,
		IteratorException,
		ConstructPageException,
		PinPageException,
		UnpinPageException
    {
		try {
			BTFileScan fileScan = new BTFileScan();
			KeyDataEntry keydata = null;
    		
			if (!open)
				error("B+Tree is closed");
				
			RID rid = new RID();

			if(_headerPage == null || _headerPage.get_rootId().pid == INVALID_PAGE)
			{
				return new BTFileScan();
			} else {
				fileScan.bfile = new BTreeFile(_filename);
				fileScan.endkey = hi_key;
				fileScan.treeFilename = _filename;
				fileScan.deletedcurrent = false;
				fileScan.didfirst = false;
				fileScan.keyType = _headerPage.get_keyType();
				fileScan.maxKeysize = _headerPage.get_maxKeySize();

				// -----------------------
				// set: curRid, leafPage
				// -----------------------

				// 1: find the leftmost leafPage
				BTLeafPage leafPage = getFirstLeafPage();

				if (leafPage==null)
					return new BTFileScan();

				keydata = leafPage.getFirst(rid);
				
				if (lo_key==null)
					lo_key = new IntegerKey(0);
				
				// 2: search through all leafPages until we find a key >= lo_key ...
				// 3: then set RID and pageNo to that entry...
				while (keydata!=null){
					if ( BT.keyCompare(keydata.key, lo_key) >= 0 ){
						fileScan.curRid = rid;
						fileScan.leafPage = leafPage;
						keydata=null;
					} else {
						keydata = leafPage.getNext(rid);
						
						if (keydata==null){
							leafPage = getNextLeafPage(leafPage);
							if (leafPage!=null)
								keydata = leafPage.getFirst(rid);
						}
					}
				}

				// we leave the page pinned when we return!	
				return fileScan;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}



	// ------------------------------------------
	// delete an entry from the B+Tree
	// ------------------------------------------
	public boolean Delete(KeyClass key, RID rid)
	throws  DeleteFashionException,
		LeafRedistributeException,
		RedistributeException,
		InsertRecException,
		KeyNotMatchException,
		UnpinPageException,
		IndexInsertRecException,
		FreePageException,
		RecordNotFoundException,
		PinPageException,
		IndexFullDeleteException,
		LeafDeleteException,
		IteratorException,
		ConstructPageException,
		DeleteRecException,
		IndexSearchException,
		IOException
	{
		try {
			boolean deleted = false;
	
			if (!open)
				error("B+Tree is closed");
	
			debug("delete()");
	
			BTSortedPage page = new BTSortedPage(_headerPage.get_rootId(), _headerPage.get_keyType());
	
			// recursiveOperation will unpin the page, so we don't have to think about that
			deleted = recursiveOperation("delete", page, key, rid);
			
			if (deleted) {
				debug("Delete returned OK");
			} else {
				debug("Unable to delete record with key #"+key);
			}
	
			redistribute(new BTSortedPage(_headerPage.get_rootId(), _headerPage.get_keyType()) );
			
			return deleted;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
    }

	

	// ---------------------------------------------------------------------
	// go recursvie through all pages until we find the right leaf page,
	// then (try to) delete/insert the entry
	// ---------------------------------------------------------------------
	public boolean recursiveOperation(String str, BTSortedPage page, KeyClass key, RID rid)
	{
		try 
		{
			boolean delete = false;		// do a delete?
			boolean insert = false;		// do a insert?
			boolean returnValue = false;
			RID curRid = new RID();
			BTSortedPage leftNode;

			debug("recursiveOperation("+str+")");

			if (str.equals("delete"))
				delete = true;
			else if (str.equals("insert"))
				insert = true;
			else
				error("unknown operation '"+str+"'");
			
			// "Index" page: pagetype==4, "Node" page: pagetype==8.
			if (getPageType(page)==4){

				// unpin page - reopen it as an IndexPage
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				BTIndexPage indexPage = new BTIndexPage(_headerPage.get_rootId(), _headerPage.get_keyType());
				
				KeyDataEntry keydata = indexPage.getFirst(curRid);
				KeyDataEntry theKeyData = null;

				// go through all entries in the index page, find the right one for our insertion
				while (keydata != null){

					if( BT.keyCompare(key, keydata.key) < 0 ){
						debug("<--- we continue to the left ("+key+"<"+keydata.key+")");
						
						// less than first record, have to follow left link
						if (theKeyData==null){
							leftNode = new BTSortedPage(indexPage.getLeftLink(), _headerPage.get_keyType());
						} else {
							leftNode = new BTSortedPage(keyData2Pid(theKeyData), _headerPage.get_keyType());
						}
							
						returnValue = recursiveOperation(str, leftNode, key, rid);

						// if returnValue = false and "_pushUpKey"
						// insert "_pushUpKey" into indexPage, reset it and return true
						if (!returnValue && _pushUpKey!=null){
							RID insertedRid = indexPage.insertKey(_pushUpKey.key, keyData2Pid(_pushUpKey));							
							if (insertedRid == null){
								error("Record is not inserted into index page - guess it's full, handle that later...!");
							} else {
								debug("key #"+_pushUpKey.key+" is inserted into indexpage no "+Integer.toString(indexPage.getCurPage().pid));
								_pushUpKey = null;
								returnValue = true;
							}
						}
						
						keydata = null;
						theKeyData = null;
					} else {
						theKeyData = keydata;
						keydata = indexPage.getNext(curRid);
					}
				}
				
				if (theKeyData != null) {					
					debug("we continue to the right --->");
					BTSortedPage rightNode = new BTSortedPage(keyData2Pid(theKeyData), _headerPage.get_keyType());
					returnValue = recursiveOperation(str, rightNode, key, rid);

					// if returnValue = false and "_pushUpKey"
					// insert "_pushUpKey" into indexPage, reset it and return true
					if (!returnValue && _pushUpKey!=null){
						RID insertedRid = indexPage.insertKey(_pushUpKey.key, keyData2Pid(_pushUpKey));							
						if (insertedRid == null){
							error("Record is not inserted into index page - guess it's full, handle that later...!");
						} else {
							debug("key #"+_pushUpKey.key+" is inserted into indexpage no "+Integer.toString(indexPage.getCurPage().pid));
							_pushUpKey = null;
							returnValue = true;
						}
					}
					keydata = null;
					theKeyData = null;
				}

				// close/unpin index page
				SystemDefs.JavabaseBM.unpinPage(indexPage.getCurPage(), true);

			} 
			else if (getPageType(page)==8)
			{
				// unpin page - reopen it as an LeafPage
				PageId pid = new PageId(page.getCurPage().pid);
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				BTLeafPage leafPage = new BTLeafPage(pid, _headerPage.get_keyType());

				// delete the entry from the node
				debug("key #"+key+" is tried "+str+" from/in page no "+Integer.toString(leafPage.getCurPage().pid));
				if (delete) {
					returnValue = leafPage.delEntry(new KeyDataEntry(key,rid));
				} else if (insert) {
					returnValue = insertIntoNode(leafPage, key, rid);
				}
				SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), false);

				pageStat(leafPage, "leafPage after "+str);

			}
			else {
				error("unidentified pagetype"); 
			}
			return returnValue;

		} catch (Exception e) { 
			e.printStackTrace(); 
		}
		return false;
	}





	// --------------------------------------------------------------
	// split a node into two (old one is devided by two, new one
	// is returned as an argument
	// --------------------------------------------------------------
	public BTLeafPage splitNode(BTLeafPage fullNode)
	{
		debug("splitNode");
		try{
			int norecords = fullNode.numberOfRecords();
			KeyDataEntry theRecord = null;
			int i;
			boolean first = true;
			RID rid = new RID();
			BTLeafPage newNode = new BTLeafPage(_headerPage.get_keyType());

			debug("Number of records in full node: "+norecords);
			
			for (i=0; i<norecords; i++){
				if (first){
					theRecord = fullNode.getFirst(rid);
					first = false;
				} else {
					theRecord = fullNode.getNext(rid);
					if (theRecord==null){
						debug("No more records in old node, return...");
						break;
					}
				}
				
				// if we are in the second half of the records
				// then we insert them into our new leafpage
				if (i>=norecords/2)
				{
					newNode.insertRecord(theRecord);
					debug("record #"+i+" with key="+theRecord.key+" is inserted into new node");
				} else {
					debug("record #"+i+" with key="+theRecord.key+" is kept in old node");
				}
			}
			
			// Delete all records in newLeafPage from oldLeafPage
			// (Didn't manage to do this at the same time as I
			// traversed through the fullnode)
			first = true;
			for (i=0; i<norecords; i++){
				if (first){
					theRecord = newNode.getFirst(rid);
					first = false;
				} else {
					theRecord = newNode.getNext(rid);
					if (theRecord==null){
						debug("No more records in old node, return...");
						break;
					}
				}
				fullNode.delEntry(theRecord);
			}
			
			debug("#"+i+" records was deleted from oldLeafPage");
			
			return newNode;
			
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
   }


	// ----------------------------------------
	// insert a key,rid pair into a node
	// ----------------------------------------
	boolean insertIntoNode(BTLeafPage leafPage, KeyClass key, RID rid){
		try{
			boolean returnValue = false;
			debug("insertIntoNode()");

			RID insertedRid = null;
				
			insertedRid = leafPage.insertRecord(key, rid);
			if (insertedRid == null){
				debug("leaf page is full, have to split and push up new key!");

				// split the full leaf page into two new leafpages
				BTLeafPage newLeafPage = splitNode(leafPage);

				// update the double linked liste, nb: the previous page 
				// for the old leafPage is not changed!
				newLeafPage.setNextPage(leafPage.getNextPage());
				newLeafPage.setPrevPage(leafPage.getCurPage());

				leafPage.setNextPage(newLeafPage.getCurPage());

				// insert the new record into new node if equal to, or bigger
				// then the first entry in the new leaf page (which is the same
				// as the value we copy up into our indexpage)
				if(BT.keyCompare(key, newLeafPage.getFirst(new RID()).key) >= 0 )
				{
					debug("record inserted into the new (rightmost) node");
					newLeafPage.insertRecord(key, rid);
				}
				// else it's less than our value copied ut in the indexpage. 
				// insert record in the old leafpage
				else
				{
					debug("record inserted into the old (leftmost) node");
					leafPage.insertRecord(key, rid);
				}

				// the key for the indexpage is the
				// first record of the newly created leafpage
				_pushUpKey = new KeyDataEntry(newLeafPage.getFirst(rid).key, newLeafPage.getCurPage());

				// unpin the new leafpage, old one will be unpinned in upper-level-call
				SystemDefs.JavabaseBM.unpinPage(newLeafPage.getCurPage(), true);

				debug("key number "+newLeafPage.getFirst(rid).key+" is pushed up");
				
				returnValue = false;
			} else {
				returnValue = true;
			}

			return returnValue;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}



	// -----------------------------------------------------------------
	// go through B+Tree and redistribute all the entries
	// -----------------------------------------------------------------
	void redistribute (BTSortedPage page)
	{
		try {
			KeyDataEntry keydata = null;
	
			debug("redistribute()");
			LinkedList records =  getAllRecords(page);

			// remove old rootpage
			_headerPage.set_rootId(new PageId(INVALID_PAGE));

			if (records != null){
	
				ListIterator iterator = records.listIterator();

				while(iterator.hasNext())
				{
					keydata = (KeyDataEntry)iterator.next();
					//if (debug) { System.out.print(keydata.key+" - "); }

					insert(keydata.key, keyData2Rid(keydata));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	// ------------------------------------------------
	// get all records from the B+Tree
	// ------------------------------------------------
	LinkedList getAllRecords (BTSortedPage page)
	{
		try {
			RID curRid = new RID();
			BTIndexPage indexPage = null;
			LinkedList records = null;

			debug("getAllRecords(#"+Integer.toString(page.getCurPage().pid)+")");

			// "Index" page: pagetype==4, "Node" page: pagetype==8.
			if (getPageType(page)==4){
	
				// unpin page - reopen it as an IndexPage
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				indexPage = new BTIndexPage(_headerPage.get_rootId(), _headerPage.get_keyType());
				
				KeyDataEntry keydata = indexPage.getFirst(curRid);
				BTSortedPage nextPage = null;

				// go through all entries in the index page
				while (keydata != null){

					// first time "nextPage" is null, then we have to follow leftLink as well
					if (nextPage == null){
						debug("page #"+Integer.toString(page.getCurPage().pid)+" has a left link to page #"+Integer.toString(indexPage.getLeftLink().pid));
						records = getAllRecords( new BTSortedPage(indexPage.getLeftLink(), _headerPage.get_keyType()) );
					}
					nextPage = new BTSortedPage(keyData2Pid(keydata), _headerPage.get_keyType());
					records.addAll(getAllRecords(nextPage));

					keydata = indexPage.getNext(curRid);

				}

				// close/unpin index page
				SystemDefs.JavabaseBM.unpinPage(indexPage.getCurPage(), true);
				
				// free the page from the buffermanager
				SystemDefs.JavabaseBM.freePage(indexPage.getCurPage());

			} 
			else if (getPageType(page)==8)
			{
				
				// unpin page - reopen it as an LeafPage
				PageId pid = new PageId(page.getCurPage().pid);
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				BTLeafPage leafPage = new BTLeafPage(pid, _headerPage.get_keyType());
				records = new LinkedList();

				KeyDataEntry keydata = leafPage.getFirst(curRid);

				// go through all entries in the leaf page
				while (keydata != null){
					records.add(keydata);					
					keydata = leafPage.getNext(curRid);
				}
				SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), false);

				// free the page from the buffermanager
				SystemDefs.JavabaseBM.freePage(leafPage.getCurPage());

			}
			else {
				error("unidentified pagetype"); 
			}

			return records;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}



	// -----------------------------------------------------
	// get the first (leftmost) leafPage in the B+Tree
	// -----------------------------------------------------
	BTLeafPage getFirstLeafPage() {
		try {

			BTSortedPage page = new BTSortedPage(_headerPage.get_rootId(), _headerPage.get_keyType());
			BTLeafPage leafPage = null;
			BTIndexPage indexPage = null;
			PageId pid = null;
			boolean go = true;
			
			// "Index" page: pagetype==4, "Node" page: pagetype==8.
			if ( getPageType(page)==8 ){

				// unpin page - reopen it as a LeafPage
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				leafPage = new BTLeafPage(_headerPage.get_rootId(), _headerPage.get_keyType());
				
			} else if ( getPageType(page)==4 ) {

				// unpin page - reopen it as a IndexPage
				pid = page.getCurPage();
				SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
				indexPage = new BTIndexPage(pid, _headerPage.get_keyType());
								
				while (go) {
					// open the indexPage left link
					page = new BTSortedPage(indexPage.getLeftLink(), _headerPage.get_keyType());
					// unpin old indexPage
					SystemDefs.JavabaseBM.unpinPage(indexPage.getCurPage(), false);

					if ( getPageType(page)==8 ){
						// unpin page - reopen it as a LeafPage
						SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
						leafPage = new BTLeafPage(_headerPage.get_rootId(), _headerPage.get_keyType());
						go = false;
					} else {
						pid = page.getCurPage();
						SystemDefs.JavabaseBM.unpinPage(page.getCurPage(), false);
						indexPage = new BTIndexPage(pid, _headerPage.get_keyType());
					}
				}				
			}

			return leafPage;
	
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}




	// ---------------------------------------------------------------
	// returns a leafpage's next leaf page (page to the right)
	// ---------------------------------------------------------------
	BTLeafPage getNextLeafPage(BTLeafPage leafPage)
	{
		try {
			BTLeafPage nextLeafPage = null;
			
			if (leafPage.getNextPage().pid!=INVALID_PAGE)
				nextLeafPage = new BTLeafPage(leafPage.getNextPage(), _headerPage.get_keyType());
			
			SystemDefs.JavabaseBM.unpinPage(leafPage.getCurPage(), false);
			return nextLeafPage;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}




	// -----------------------------------------------------
	// return page type, 4 for IndexPage, 8 for LeafPage
	// -----------------------------------------------------
	int getPageType(BTSortedPage page){
		try {
			return BT.getDataLength(page.getType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	// ---------------------------------------------
	// converts keydata into PageId
	// ---------------------------------------------
	PageId keyData2Pid(KeyDataEntry keydata){
		return new PageId(Integer.valueOf(((IndexData)keydata.data).toString()).intValue());
	}

	// ---------------------------------------------
	// converts keydata into RID
	// ---------------------------------------------
	RID keyData2Rid(KeyDataEntry keydata){
		String pageNo = parse(keydata.data.toString(), " ")[1];
		String slotNo = parse(keydata.data.toString(), " ")[2];
		return new RID(new PageId(Integer.parseInt(pageNo)), Integer.parseInt(slotNo));
	}


	// --------------------------------------------
	// methods for debugging/printing etc...
	// --------------------------------------------
	void debug (String str)
	{
		if (debug)
			System.out.println("DEBUG: "+str);
	}

	void print (String str)
	{
		System.out.println(str);	
	}

	void error (String str)
	{
		System.out.println("ERROR: "+str);
		System.exit(1);
	}

	void pageStat (BTSortedPage page, String str){
		try{
			if (str == null)
				str = "";
			else
				str = " ["+str+"]";

			debug("# records: "+Integer.toString(page.numberOfRecords())+" avail. space: "+Integer.toString(page.available_space())+str);
		} catch (Exception e){
			e.printStackTrace();
		}
	}


    String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }

		return tokens;
    }


}

